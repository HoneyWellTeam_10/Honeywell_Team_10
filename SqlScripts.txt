create table city (id int not null AUTO_INCREMENT, name varchar(255), PRIMARY KEY(id));

create table area (id int not null AUTO_INCREMENT, name varchar(255), city_id int, PRIMARY KEY(id), FOREIGN KEY (city_id) REFERENCES city(id));

create table hotel (id int not null AUTO_INCREMENT, name varchar(255), address varchar(255), rating int, area_id int, PRIMARY KEY(id), FOREIGN KEY (area_id) REFERENCES area(id));

create table menu_category (id int not null AUTO_INCREMENT, name varchar(255),  PRIMARY KEY(id));

create table menu_item (id int not null AUTO_INCREMENT, name varchar(255), menu_category_id int, description text, FOREIGN KEY (menu_category_id) REFERENCES menu_category(id), primary key(id));

create table hotel_item (id int not null AUTO_INCREMENT, price dec, menu_item_id int, description text, FOREIGN KEY (menu_item_id) REFERENCES menu_item(id), primary key(id) );